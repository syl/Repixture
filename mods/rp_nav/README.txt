Navigation mod
==============
By Kaadmy and Wuzzy, for Repixture.

Navigation, compass and waypoint API

Asset license: CC BY-SA 4.0
Source license: LGPLv2.1
