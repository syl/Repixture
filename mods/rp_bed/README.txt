Beds mod
========
Originally by PilzAdam, thefamilygrog66.
Tweaked by Kaadmy and Wuzzy, for Repixture.

Use the 'place' key on a bed to sleep, use the same key again to get out.

## Developers

See `API.md`.

## Licensing

Asset license: CC BY-SA 4.0
Source licens: LGPLv2.1
