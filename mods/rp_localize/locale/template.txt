# textdomain: rp_localize
∞=
#Negative number
−@1=
#Same as above, but with a minus sign in front
−@1.@2=
#Translation of the decimal point. @1 and @2 are the number parts
#in front and behind the decimal point.
@1.@2=
