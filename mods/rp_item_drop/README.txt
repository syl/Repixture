Item drop mod
=============

Adds custom item drop handling.

If a node is dug, its drop will appear as items on the ground.

Developers: There's a special function to "simulate" an item drop (see API.md).

## License
Credits: Originally by PilzAdam, then
tweaked by Kaadmy for Pixture and later Wuzzy for Repixture.

Source license: LGPLv2.1
