Music player mod
================
By Kaadmy and Wuzzy, for Repixture.

Music license:
    music_catsong: Dan Knoflicek(CC0)
    music_greyarms.ogg: Tozan(CC0)

Texture license: CC BY-SA 4.0
Source License: LGPLv2.1
